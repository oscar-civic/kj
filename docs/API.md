## 

* [Events](#Events)
    * [.clear()](#Events+clear) ⇒ <code>this</code>
    * [.on(event_reference, fn, [once])](#Events+on) ⇒ <code>this</code>
    * [.once(event_reference, fn)](#Events+once) ⇒ <code>this</code>
    * [.off(event_reference)](#Events+off) ⇒ <code>this</code>
    * [.trigger(event_name, ...data)](#Events+trigger) ⇒ <code>this</code>
* [TemplateLoader](#TemplateLoader)
    * [.loadBuffer(...args)](#TemplateLoader+loadBuffer) ⇒ <code>Promise</code> \| <code>Array.&lt;boolean&gt;</code>
    * [.loadLibrary(library_id)](#TemplateLoader+loadLibrary) ⇒ <code>Promise</code> \| <code>boolean</code>
* [TemplateManager](#TemplateManager)
    * [new TemplateManager()](#new_TemplateManager_new)
    * [.compile(buffer)](#TemplateManager.compile) ⇒ <code>function</code>
    * [.getBuffer(template_id)](#TemplateManager.getBuffer) ⇒ <code>string</code> \| <code>null</code>
    * [.registerPartial(template_id)](#TemplateManager.registerPartial) ⇒ <code>string</code>
    * [.registerBuffer(name, buffer)](#TemplateManager.registerBuffer) ⇒ <code>string</code>
    * [.assignContext(template, data)](#TemplateManager.assignContext) ⇒ <code>string</code> \| <code>null</code>
* [Element](#Element)
    * [new Element(tag_name, template, data, options)](#new_Element_new)
    * [.initialize()](#Element+initialize) ⇒ <code>void</code>
    * [.terminate()](#Element+terminate) ⇒ <code>void</code>
    * [.getNode()](#Element+getNode) ⇒ <code>Node</code>
    * [.setTemplateBuffer(templateBuffer)](#Element+setTemplateBuffer) ⇒ <code>void</code>
    * [.getTemplateBuffer()](#Element+getTemplateBuffer) ⇒ <code>string</code>
    * [.setData(data)](#Element+setData) ⇒ <code>void</code>
    * [.getData()](#Element+getData) ⇒ <code>Object</code> \| <code>function</code>
    * [.build()](#Element+build) ⇒ <code>void</code>
    * [.addSubscription(event_object, event_name)](#Element+addSubscription) ⇒ <code>void</code>
    * [.toString()](#Element+toString) ⇒ <code>string</code>
* [fn](#fn)
    * [new fn()](#new_fn_new)
    * [.logDebug(...args)](#fn.logDebug) ⇒ <code>void</code>
    * [.logWarning(...warning_object)](#fn.logWarning) ⇒ <code>void</code>
    * [.logError(error_object, throw_exception)](#fn.logError) ⇒ <code>void</code>
    * [.isFunction(data)](#fn.isFunction) ⇒ <code>boolean</code>
    * [.isArray(data)](#fn.isArray) ⇒ <code>boolean</code>
    * [.isString(data)](#fn.isString) ⇒ <code>boolean</code>
    * [.isObject(data)](#fn.isObject) ⇒ <code>boolean</code>
    * [.isNull(data)](#fn.isNull) ⇒ <code>boolean</code>
    * [.getUniqueId()](#fn.getUniqueId) ⇒ <code>string</code>
    * [.generateToken([len])](#fn.generateToken) ⇒ <code>string</code>
    * [.escapeRegExp(string)](#fn.escapeRegExp)
    * [.createElement(tag_name, inner_html, attributes, properties)](#fn.createElement)
    * [.registerBinding(item, recipient)](#fn.registerBinding) ⇒ <code>void</code>

## 

* [module_options](#module_options) : <code>Object</code>
* [kj](#kj) : <code>Object</code>

<a name="Events"></a>

## Events
<p>Gestión de eventos</p>

**Kind**: global class  

* [Events](#Events)
    * [.clear()](#Events+clear) ⇒ <code>this</code>
    * [.on(event_reference, fn, [once])](#Events+on) ⇒ <code>this</code>
    * [.once(event_reference, fn)](#Events+once) ⇒ <code>this</code>
    * [.off(event_reference)](#Events+off) ⇒ <code>this</code>
    * [.trigger(event_name, ...data)](#Events+trigger) ⇒ <code>this</code>


* * *

<a name="Events+clear"></a>

### events.clear() ⇒ <code>this</code>
<p>Limpia la lista de eventos</p>

**Kind**: instance method of [<code>Events</code>](#Events)  

* * *

<a name="Events+on"></a>

### events.on(event_reference, fn, [once]) ⇒ <code>this</code>
<p>Suscribe un evento</p>

**Kind**: instance method of [<code>Events</code>](#Events)  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| event_reference | <code>string</code> |  | <p>Nombre del evento</p> |
| fn | <code>function</code> |  | <p>Funcion que se ejecutura</p> |
| [once] | <code>boolean</code> | <code>false</code> | <p>Solo se tiene que ejecutar una vez</p> |


* * *

<a name="Events+once"></a>

### events.once(event_reference, fn) ⇒ <code>this</code>
<p>Suscribe un evento para una única ejecución</p>

**Kind**: instance method of [<code>Events</code>](#Events)  

| Param | Type | Description |
| --- | --- | --- |
| event_reference | <code>string</code> | <p>Nombre del evento</p> |
| fn | <code>function</code> | <p>Funcion que se ejecutura</p> |


* * *

<a name="Events+off"></a>

### events.off(event_reference) ⇒ <code>this</code>
<p>Desuscribe un evento</p>

**Kind**: instance method of [<code>Events</code>](#Events)  

| Param | Type | Description |
| --- | --- | --- |
| event_reference | <code>string</code> | <p>Nombre del evento</p> |


* * *

<a name="Events+trigger"></a>

### events.trigger(event_name, ...data) ⇒ <code>this</code>
<p>Dispara un evento y ejecuta todas las acciones suscritas al
evento disparado</p>

**Kind**: instance method of [<code>Events</code>](#Events)  

| Param | Type | Description |
| --- | --- | --- |
| event_name | <code>string</code> | <p>Nombre del evento</p> |
| ...data | <code>\*</code> | <p>Cualquier dato extra que necesite el evento</p> |


* * *

<a name="TemplateLoader"></a>

## TemplateLoader
<p>Herramienta para cargar las plantillas desde el DOM o el servidor al localStorage</p>

**Kind**: global class  

* [TemplateLoader](#TemplateLoader)
    * [.loadBuffer(...args)](#TemplateLoader+loadBuffer) ⇒ <code>Promise</code> \| <code>Array.&lt;boolean&gt;</code>
    * [.loadLibrary(library_id)](#TemplateLoader+loadLibrary) ⇒ <code>Promise</code> \| <code>boolean</code>


* * *

<a name="TemplateLoader+loadBuffer"></a>

### templateLoader.loadBuffer(...args) ⇒ <code>Promise</code> \| <code>Array.&lt;boolean&gt;</code>
<p>Obtiene el buffer de una o más plantillas.
Comprueba que no estén ya cargadas, las busca en el DOM actual o las carga desde el servidor
y las deja en el localStorage</p>

**Kind**: instance method of [<code>TemplateLoader</code>](#TemplateLoader)  

| Param | Type | Description |
| --- | --- | --- |
| ...args | <code>string</code> | <p>Ids de plantilla</p> |


* * *

<a name="TemplateLoader+loadLibrary"></a>

### templateLoader.loadLibrary(library_id) ⇒ <code>Promise</code> \| <code>boolean</code>
<p>Carga una libreria de plantillas</p>

**Kind**: instance method of [<code>TemplateLoader</code>](#TemplateLoader)  

| Param | Type | Description |
| --- | --- | --- |
| library_id | <code>string</code> | <p>Identificador de la librería</p> |


* * *

<a name="TemplateManager"></a>

## TemplateManager
<p>Herramienta para el manejo de plantillas. Recupera el buffer de una plantilla previamente cargada
en el localStorage. Provee además de herramientas para compilarla, asignar contextos y registrar parciales.</p>

**Kind**: global class  

* [TemplateManager](#TemplateManager)
    * [new TemplateManager()](#new_TemplateManager_new)
    * [.compile(buffer)](#TemplateManager.compile) ⇒ <code>function</code>
    * [.getBuffer(template_id)](#TemplateManager.getBuffer) ⇒ <code>string</code> \| <code>null</code>
    * [.registerPartial(template_id)](#TemplateManager.registerPartial) ⇒ <code>string</code>
    * [.registerBuffer(name, buffer)](#TemplateManager.registerBuffer) ⇒ <code>string</code>
    * [.assignContext(template, data)](#TemplateManager.assignContext) ⇒ <code>string</code> \| <code>null</code>


* * *

<a name="new_TemplateManager_new"></a>

### new TemplateManager()
<blockquote>
<p>Todos los procedimientos de la clase son estáticos. No necesitas instanciar la clase para nada</p>
</blockquote>

**Example**  
```js
// Carga el buffer de una plantilla desde el localStorage
const buffer = kj.templateManager.getBuffer('template_id');
// Compila una plantilla y la deja lista para usar
const handler = kj.templateManager.compile(buffer);
// Asigna contexto al handler creado previamente y genera el buffer final
const html_string = kj.templateManager.assignContext(handler, {example: 'example'});
```

* * *

<a name="TemplateManager.compile"></a>

### TemplateManager.compile(buffer) ⇒ <code>function</code>
<p>Compila una plantilla</p>

**Kind**: static method of [<code>TemplateManager</code>](#TemplateManager)  
**Returns**: <code>function</code> - <p>Plantilla compilada</p>  

| Param | Type | Description |
| --- | --- | --- |
| buffer | <code>string</code> | <p>Buffer con la plantilla</p> |


* * *

<a name="TemplateManager.getBuffer"></a>

### TemplateManager.getBuffer(template_id) ⇒ <code>string</code> \| <code>null</code>
<p>Retorna el buffer de la plantilla, el buffer debe estar en el localStorage</p>

**Kind**: static method of [<code>TemplateManager</code>](#TemplateManager)  

| Param | Type |
| --- | --- |
| template_id | <code>string</code> | 


* * *

<a name="TemplateManager.registerPartial"></a>

### TemplateManager.registerPartial(template_id) ⇒ <code>string</code>
<p>Registra la plantilla como parcial</p>

**Kind**: static method of [<code>TemplateManager</code>](#TemplateManager)  

| Param | Type |
| --- | --- |
| template_id | <code>string</code> | 


* * *

<a name="TemplateManager.registerBuffer"></a>

### TemplateManager.registerBuffer(name, buffer) ⇒ <code>string</code>
<p>Registra el buffer como parcial</p>

**Kind**: static method of [<code>TemplateManager</code>](#TemplateManager)  

| Param | Type |
| --- | --- |
| name | <code>string</code> | 
| buffer | <code>string</code> | 


* * *

<a name="TemplateManager.assignContext"></a>

### TemplateManager.assignContext(template, data) ⇒ <code>string</code> \| <code>null</code>
<p>Asigna contexto a una plantilla y retorna el resultado</p>

**Kind**: static method of [<code>TemplateManager</code>](#TemplateManager)  

| Param | Type | Description |
| --- | --- | --- |
| template | <code>function</code> | <p>Plantilla compilada</p> |
| data | <code>Object</code> | <p>Contexto</p> |


* * *

<a name="Element"></a>

## Element
<p>Generación de contenido HTML a partir de una plantilla y un contexto.</p>

**Kind**: global class  

* [Element](#Element)
    * [new Element(tag_name, template, data, options)](#new_Element_new)
    * [.initialize()](#Element+initialize) ⇒ <code>void</code>
    * [.terminate()](#Element+terminate) ⇒ <code>void</code>
    * [.getNode()](#Element+getNode) ⇒ <code>Node</code>
    * [.setTemplateBuffer(templateBuffer)](#Element+setTemplateBuffer) ⇒ <code>void</code>
    * [.getTemplateBuffer()](#Element+getTemplateBuffer) ⇒ <code>string</code>
    * [.setData(data)](#Element+setData) ⇒ <code>void</code>
    * [.getData()](#Element+getData) ⇒ <code>Object</code> \| <code>function</code>
    * [.build()](#Element+build) ⇒ <code>void</code>
    * [.addSubscription(event_object, event_name)](#Element+addSubscription) ⇒ <code>void</code>
    * [.toString()](#Element+toString) ⇒ <code>string</code>


* * *

<a name="new_Element_new"></a>

### new Element(tag_name, template, data, options)
<p>Para personalizar el comportamiento del Element se pueden sobreescribir
los siguientes métodos, que por defecto no hacen nada:</p>
<ul>
<li>beforeInitialize</li>
<li>beforeBuild  </li>
<li>afterBuild</li>
<li>afterInitialize</li>
<li>onTerminate</li>
<li>onConnect</li>
<li>onDisconnect</li>
</ul>


| Param | Type | Default | Description |
| --- | --- | --- | --- |
| tag_name | <code>string</code> \| <code>Node</code> |  | <p>Tag del Nodo principal, o el propio nodo si ya existe</p> |
| template | <code>string</code> \| <code>function</code> | <code>null</code> | <p>Buffer de plantilla o plantilla compilada</p> |
| data | <code>Object</code> \| <code>function</code> | <code></code> | <p>Datos usados como contexto o callback que retorne los datos</p> |
| options | <code>Object</code> | <code>{}</code> | <p>Opciones de configuración</p> |
| options.auto_initialize | <code>boolean</code> | <code>true</code> | <p>Ejecuta el procedimiento de incialización al instanciar</p> |
| options.enable_observer | <code>boolean</code> | <code>false</code> | <p>Habilita la funcionalidad del DomObserver</p> |
| options.add_kjid | <code>boolean</code> | <code>false</code> | <p>Añade el atributo data-kjid con id único</p> |
| options.attr | <code>Object</code> | <code>{}</code> | <p>Atributos para le nodo principal</p> |
| options.prop | <code>Object</code> | <code>{}</code> | <p>Propiedades para le nodo principal</p> |
| options.bind | <code>Object</code> | <code>{}</code> | <p>Se extiende la instancia recien creada con lo que haya en este campo</p> |
| options.on_initialized | <code>callback</code> |  | <p>Se ejecuta una vez inicializado el Element</p> |
| options.on_built | <code>callback</code> |  | <p>Se ejecuta cada vez que se genera e inserta nuevo contenido</p> |
| options.on_disconnected | <code>callback</code> |  | <p>Se ejecuta cuando se desconecta el Nodo de la página</p> |
| options.on_connected | <code>callback</code> |  | <p>Se ejecuta cuando se conecta el Nodo a la página</p> |
| options.on_terminated | <code>callback</code> |  | <p>Se ejecuta cuando el element se ha finalizado</p> |

**Example**  
```js
todo = new kj.Element('ul', `
  {{#todoList}}
      <li>{{.}}</li>
  {{/todoList}}`, {
      todoList: [
          'Primera cosa de la lista',
          'Segunda cosa de la lista'
      ]
  });

//  Genera:
//  <ul>
//    <li>Primera cosa de la lista</li>
//    <li>Segunda cosa de la lista</li>
//  </ul>
```

* * *

<a name="Element+initialize"></a>

### element.initialize() ⇒ <code>void</code>
<p>Inicializa el element</p>

**Kind**: instance method of [<code>Element</code>](#Element)  
**Emits**: <code>event:beforeInitialize Antes de inicializar el Element</code>, <code>event:beforeBuild Una vez creado el Nodo principal y antes de insertarle en contenido</code>, <code>event:afterBuild Inmediatamente después de insertar el contenido y registrar los binding definidos en la plantilla</code>, <code>event:onBuilt Callback definido en las opciones del constructor</code>, <code>event:afterInitialize Una vez inicializado el Element</code>, <code>event:onInitialized Callback definido en las opciones del constructor</code>  

* * *

<a name="Element+terminate"></a>

### element.terminate() ⇒ <code>void</code>
<p>Realiza tareas de finalización y limpieza, elimina el HTML de la página</p>

**Kind**: instance method of [<code>Element</code>](#Element)  
**Emits**: <code>event:onTerminate Una vez finalizado el Element</code>, <code>event:onTerminated Callback definido en las opciones</code>, <code>onDisconnect Inmediatamente después de haberse desconectado (condicional)</code>, <code>onDisconnected Callback definido en las opciones (condicional)</code>  

* * *

<a name="Element+getNode"></a>

### element.getNode() ⇒ <code>Node</code>
<p>Retorna el Node generado</p>

**Kind**: instance method of [<code>Element</code>](#Element)  

* * *

<a name="Element+setTemplateBuffer"></a>

### element.setTemplateBuffer(templateBuffer) ⇒ <code>void</code>
<p>Asigna buffer de plantilla</p>

**Kind**: instance method of [<code>Element</code>](#Element)  

| Param | Type | Description |
| --- | --- | --- |
| templateBuffer | <code>string</code> \| <code>function</code> | <p>Buffer de la plantilla</p> |


* * *

<a name="Element+getTemplateBuffer"></a>

### element.getTemplateBuffer() ⇒ <code>string</code>
<p>Retorna el buffer de la plantilla</p>

**Kind**: instance method of [<code>Element</code>](#Element)  

* * *

<a name="Element+setData"></a>

### element.setData(data) ⇒ <code>void</code>
<p>Asigna contexto a la plantilla</p>

**Kind**: instance method of [<code>Element</code>](#Element)  

| Param | Type | Description |
| --- | --- | --- |
| data | <code>Object</code> \| <code>function</code> | <p>Mapeado de valores para el contexto</p> |


* * *

<a name="Element+getData"></a>

### element.getData() ⇒ <code>Object</code> \| <code>function</code>
<p>Retorna el contexto de la plantilla</p>

**Kind**: instance method of [<code>Element</code>](#Element)  

* * *

<a name="Element+build"></a>

### element.build() ⇒ <code>void</code>
<p>Crea el contenido del Nodo.</p>

**Kind**: instance method of [<code>Element</code>](#Element)  
**Emits**: <code>event:beforeBuild Una vez creado el Nodo principal y antes de insertarle el contenido</code>, <code>event:afterBuild Inmediatamente después de insertar el contenido y registrar los binding definidos en la plantilla</code>, <code>event:onBuilt Callback definido en las opciones del constructor</code>  

* * *

<a name="Element+addSubscription"></a>

### element.addSubscription(event_object, event_name) ⇒ <code>void</code>
<p>Añade una suscripción.<br>Cuando se dispare el evento se ejecutará el procedimiento <code>build</code> que reconstruirá el html</p>

**Kind**: instance method of [<code>Element</code>](#Element)  

| Param | Type | Description |
| --- | --- | --- |
| event_object | <code>Object</code> | <p>Cualquier objeto al que se pueda bindear un evento usando <code>on</code> o <code>addEventListener</code></p> |
| event_name | <code>string</code> | <p>Nombre del evento</p> |


* * *

<a name="Element+toString"></a>

### element.toString() ⇒ <code>string</code>
<p>String representando el elemento</p>

**Kind**: instance method of [<code>Element</code>](#Element)  

* * *

<a name="fn"></a>

## fn
<p>Librería de helpers</p>

**Kind**: global class  

* [fn](#fn)
    * [new fn()](#new_fn_new)
    * [.logDebug(...args)](#fn.logDebug) ⇒ <code>void</code>
    * [.logWarning(...warning_object)](#fn.logWarning) ⇒ <code>void</code>
    * [.logError(error_object, throw_exception)](#fn.logError) ⇒ <code>void</code>
    * [.isFunction(data)](#fn.isFunction) ⇒ <code>boolean</code>
    * [.isArray(data)](#fn.isArray) ⇒ <code>boolean</code>
    * [.isString(data)](#fn.isString) ⇒ <code>boolean</code>
    * [.isObject(data)](#fn.isObject) ⇒ <code>boolean</code>
    * [.isNull(data)](#fn.isNull) ⇒ <code>boolean</code>
    * [.getUniqueId()](#fn.getUniqueId) ⇒ <code>string</code>
    * [.generateToken([len])](#fn.generateToken) ⇒ <code>string</code>
    * [.escapeRegExp(string)](#fn.escapeRegExp)
    * [.createElement(tag_name, inner_html, attributes, properties)](#fn.createElement)
    * [.registerBinding(item, recipient)](#fn.registerBinding) ⇒ <code>void</code>


* * *

<a name="new_fn_new"></a>

### new fn()
<blockquote>
<p>Todos los procedimientos de la clase son estáticos. No necesitas instanciar la clase para nada</p>
</blockquote>


* * *

<a name="fn.logDebug"></a>

### fn.logDebug(...args) ⇒ <code>void</code>
<p>Log de debug</p>

**Kind**: static method of [<code>fn</code>](#fn)  

| Param | Type | Description |
| --- | --- | --- |
| ...args | <code>\*</code> | <p>Datos de debug</p> |


* * *

<a name="fn.logWarning"></a>

### fn.logWarning(...warning_object) ⇒ <code>void</code>
<p>Log de warning</p>

**Kind**: static method of [<code>fn</code>](#fn)  

| Param | Type | Description |
| --- | --- | --- |
| ...warning_object | <code>\*</code> | <p>Warning</p> |


* * *

<a name="fn.logError"></a>

### fn.logError(error_object, throw_exception) ⇒ <code>void</code>
<p>Log de error</p>

**Kind**: static method of [<code>fn</code>](#fn)  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| error_object | <code>\*</code> |  | <p>Error</p> |
| throw_exception | <code>boolean</code> | <code>false</code> | <p>Lanza excepción</p> |


* * *

<a name="fn.isFunction"></a>

### fn.isFunction(data) ⇒ <code>boolean</code>
<p>Retorna true si data es una función</p>

**Kind**: static method of [<code>fn</code>](#fn)  

| Param | Type | Description |
| --- | --- | --- |
| data | <code>\*</code> | <p>Dato</p> |


* * *

<a name="fn.isArray"></a>

### fn.isArray(data) ⇒ <code>boolean</code>
<p>Retorna true si data es un array</p>

**Kind**: static method of [<code>fn</code>](#fn)  

| Param | Type | Description |
| --- | --- | --- |
| data | <code>\*</code> | <p>Dato</p> |


* * *

<a name="fn.isString"></a>

### fn.isString(data) ⇒ <code>boolean</code>
<p>Retorna true si data es un string</p>

**Kind**: static method of [<code>fn</code>](#fn)  

| Param | Type | Description |
| --- | --- | --- |
| data | <code>\*</code> | <p>Dato</p> |


* * *

<a name="fn.isObject"></a>

### fn.isObject(data) ⇒ <code>boolean</code>
<p>Retorna true si data es un objeto</p>

**Kind**: static method of [<code>fn</code>](#fn)  

| Param | Type | Description |
| --- | --- | --- |
| data | <code>\*</code> | <p>Dato</p> |


* * *

<a name="fn.isNull"></a>

### fn.isNull(data) ⇒ <code>boolean</code>
<p>Retorna true si data es null</p>

**Kind**: static method of [<code>fn</code>](#fn)  

| Param | Type | Description |
| --- | --- | --- |
| data | <code>\*</code> | <p>Dato</p> |


* * *

<a name="fn.getUniqueId"></a>

### fn.getUniqueId() ⇒ <code>string</code>
<p>Retorna una cadena para usar como id único de un objeto o elemento</p>

**Kind**: static method of [<code>fn</code>](#fn)  

* * *

<a name="fn.generateToken"></a>

### fn.generateToken([len]) ⇒ <code>string</code>
<p>Generador de palabras aleatorias de longitud específica</p>

**Kind**: static method of [<code>fn</code>](#fn)  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| [len] | <code>int</code> | <code>12</code> | <p>Longitud de la cadena generada</p> |


* * *

<a name="fn.escapeRegExp"></a>

### fn.escapeRegExp(string)
<p>Escapa la cadena para que pueda insertarse en una expresión regular</p>

**Kind**: static method of [<code>fn</code>](#fn)  

| Param | Type |
| --- | --- |
| string | <code>string</code> | 


* * *

<a name="fn.createElement"></a>

### fn.createElement(tag_name, inner_html, attributes, properties)
<p>Crea un nuevo elemento HTML</p>

**Kind**: static method of [<code>fn</code>](#fn)  

| Param | Type | Description |
| --- | --- | --- |
| tag_name | <code>string</code> \| <code>Node</code> |  |
| inner_html | <code>string</code> |  |
| attributes | <code>Object</code> | <p>Attributos del nuevo elemento</p> |
| properties | <code>Object</code> | <p>Propiedades que extienden el nuevo elemento</p> |


* * *

<a name="fn.registerBinding"></a>

### fn.registerBinding(item, recipient) ⇒ <code>void</code>
<p>Registra inline bindings  </p>
<p>Bindea de forma automágica eventos en objetos DOM contenidos en la plantilla del control.  </p>
<p>La sintaxis es:<br>data-bind=&quot;evento1 eventoN: callback&quot;, donde evento1 y eventoN son los nombres de los eventos
por ejemplo 'click','change',submit', etc y callback es el procedimiento que se ejecuta
al disparase alguno de los eventos indicados, el callback tiene que ser un procedimiento del
objeto pasado con recipient, si no, no se ejecutará.  </p>
<p>Se pueden indicar todos los calbacks que sean necesarios separando frases por &quot;;&quot; tal que:<br>data-bind=&quot;event1: callback1; event2: callback2&quot;</p>

**Kind**: static method of [<code>fn</code>](#fn)  

| Param | Type | Description |
| --- | --- | --- |
| item | <code>Node</code> | <p>Elemento HTML</p> |
| recipient | <code>\*</code> | <p>Objeto con los callbacks que se ejecutan al dispararse los diferentes eventos bindeados</p> |

**Example**  
```js
<a href="#" data-bind="click: onMyButtonClick">Button</a>
// Al hacer click en el anchor se intentará ejecutar el procedimiento `onMyButtonClick` de recipient
```

* * *

<a name="module_options"></a>

## module\_options : <code>Object</code>
<p>Opciones de configuración del módulo</p>

**Kind**: global constant  
**Properties**

| Name | Type | Default | Description |
| --- | --- | --- | --- |
| show_debug | <code>boolean</code> | <code>false</code> | <p>Imprime mensajes de debug</p> |
| show_warnings | <code>boolean</code> | <code>false</code> | <p>Imprime mensajes de advertencia</p> |
| show_errors | <code>boolean</code> | <code>false</code> | <p>Imprime mensajes de error</p> |
| use_dom_observer | <code>boolean</code> | <code>false</code> | <p>Instancia DomObserver para descubrir cambios en el DOM de la página. Es importante tener en cuenta que esta opción es crítica en cuanto a rendimiento y la inserción de elementos en la página es mucho más lenta cuando está activa.</p> |
| templates_url | <code>string</code> | <code>&quot;/js/templates/&quot;</code> | <p>URL al directorio de plantillas</p> |
| templates_query | <code>string</code> |  | <p>Query string para la peticion Ej: &quot;?name=&quot; Se insertara automaticamente el id de plantilla al final de la cadena</p> |
| templates_ext | <code>string</code> | <code>&quot;.tpl&quot;</code> | <p>Extensión para los archivos de plantilla</p> |
| templates_id_prefix | <code>string</code> | <code>&quot;template-&quot;</code> | <p>Prefijo para IDs de plantilla que se encuentran en la página</p> |
| storage_template_prefix | <code>string</code> | <code>&quot;templates.&quot;</code> | <p>Prefijo para los nombres de plantilla en el localStorage</p> |
| element_auto_initialize | <code>boolean</code> | <code>true</code> | <p>Inicializa el Element al construir la instancia</p> |
| element_enable_observer | <code>boolean</code> | <code>false</code> | <p>Bindea los eventos generados por el DomObserver para descubrir cuando se ha conectado o desconectado el Element</p> |
| element_add_kjid | <code>boolean</code> | <code>false</code> | <p>Añade el atributo data-kjid con id único a cada nodo</p> |


* * *

<a name="kj"></a>

## kj : <code>Object</code>
<p>Interface público del módulo</p>

**Kind**: global constant  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| options | <code>Object</code> | <p>Opciones del módulo ([module_options](#module_options))</p> |
| templateLoader | [<code>TemplateLoader</code>](#TemplateLoader) | <p>Instancia de TemplateLoader</p> |
| templateManager | [<code>TemplateManager</code>](#TemplateManager) | <p>Herramienta TemplateManager</p> |
| Element | [<code>Element</code>](#Element) | <p>Constructor para Element</p> |
| Events | [<code>Events</code>](#Events) | <p>Constructor para Events</p> |
| fn | [<code>fn</code>](#fn) | <p>Librería de helpers</p> |
| status | <code>Object</code> | <p>Estado del módulo</p> |
| status.runningElements | [<code>Array.&lt;Element&gt;</code>](#Element) | <p>Elements en ejecución</p> |


* * *

