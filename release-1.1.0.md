# [1.1.0] - 2021-05-25

## `Initialize` implícito  

Solución para el doble `build` en kj con construcción en el DOM virtual y posterior `setNode` para renderizarlo en el interior de un control padre  

### Patrón actual para KJ embebido

```javascript
class ControlPadre extends KJControl {
    constructor( node, text ) {
        super( node, TEMPLATE.PADRE, {title: text}, {auto_initialize: false} );
        this.initialize();
    }

    initialize() {
        this.hijo = new ControlHijo( 'div', 'Hijo' );
        super.initialize();
    }

    afterBuild() {
        super.afterBuild();
        this.hijo.setNode( kj('#hijo').first() );
    }
}

class ControlHijo extends KJControl {
    constructor( node, text ) {
        super( node, TEMPLATE.HIJO, {title: text}, {auto_initialize: false} );
        this.initialize();
    }
}
```

Ejecutar el procedimiento `initialize` en el constructor del control hijo es necesario para inicializar el kj, pero como efecto secundario provocamos un `build` innecesario, con la mejora actual podemos delegar la inicialización en el procedimiento `build` que comprueba si el kj esta inicializado y si no es así lo inicializa al vuelo.  
De esta manera, cuando sea adecuado, **podremos omitir la inicialización en el constructor y esperar a que el kj se inicialize por su cuenta.**  

### Nuevo patrón  

```javascript
class ControlHijo extends KJControl {
    constructor( node, text ) {
        super( node, TEMPLATE.HIJO, {title: text}, {auto_initialize: false} );
    }
}
```

**Este patrón no será adecuado para todos los casos, por ejemplo**  

```javascript
class ControlPadre extends KJControl {
    constructor( node, text ) {
        super( node, TEMPLATE.PADRE, {title: text}, {auto_initialize: false} );
        this.initialize();
    }

    initialize() {
        this.hijo = new ControlHijo( 'div', 'Hijo' );
        super.initialize();
    }

    afterBuild() {
        super.afterBuild();
        kj( this.getNode() ).appendChild( this.hijo.getNode() );
    }
}
```

En este caso el control padre no está forzando un `build` del control hijo, por lo que si éste no se ha inicializado, al hacer el `appendChild` sólo se estará insertando un nodo vacío  

## Sistema de mensajería  

Permite enviar datos entre kjs para facilitar el intercambio de información entre los diferentes componetes de la página.

### Envío de información  

```javascript
class KjRequest extends App.KjElement {
    /**
     * Envía los datos de la entidad a kjs con la misma instancia
     */
    syncData() {
        const kjs = this.getRunningElements().filter( kj => 
            // Seleccionamos solo los kj de la class `KjRequest`
            kj.constructor.name == 'KjRequest' 
            // Nos saltamos la instancia actual
            && kj.uniqueId != this.uniqueId
            // Seleccionamos solo los kj que representan a la misma entidad
            && kj._dataSource.get( 'id' ) == this._dataSource.get( 'id' )
        );
        if ( !kjs.length ) {
            return;
        }
        
        this.postSystemMessage( 
            new kj.Message( this, 'sync', this._dataSource.getData() ), 
            kjs
        );
    }
}
```

### Recepción de la información  

```javascript
class KjRequest extends App.KjElement {
    initialize() {
        super.initialize();
        this.onSystemMessage( 'kjrequest/sync', message => {
            this._dataSource.set( message.data );
            this.build();
        } );
    }
}
```
