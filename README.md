## kj library
**Selección de herramientas pensadas para crear y renderizar contenido en el navegador.**  

La mecánica de funcionamiento es la siguiente:  

- Se carga una plantilla previamente maquetada, bien sea desde el servidor o bien ubicada en el DOM de la página actual y se almacena en el localStorage para tenerla accesible posteriormente sin necesidad de volverla a solicitar.  
- Por otra parte tenemos los datos específicos que se quieren presentar al usuario y que se combinan con la plantilla, previamente almacenada en el localStorage, para dar a la información el aspecto deseado.  
Para presentar información de otra entidad similar o actualizar la de la actual, solo será necesario cargar desde el servidor la estructura de datos específica, inyectarla en la plantilla y volver a renderizar.  

Para realizar dichas tareas, que hemos dividido en tres partes, contamos con estas herramientas principales:  

- **TemplateLoader** Obtiene la plantilla y la almacena en el localStorage.  
- **TemplateManager** Usa el sistema de plantillas ***Handlebars*** para combinar los datos y la plantilla, y generar un buffer con el contenido final.  
- **Element** Representa cada uno de los componentes dinámicos que se presentarán en la paǵina, y cuyo uso básico es el de convertir el contenido generado al unir datos y plantilla en un elemento HTML o ***Node*** listo para ser insertado en el DOM que compone la página actual.  

### Ejemplos

```javascript
    // Instancia un Element
    const element = new kj.Element('div', 'Hola <strong>{{name}}</strong>!!', {name: 'Mundo'});
    // Inserta element en la página
    document.body.appendChild(element.getNode());
    // --> render: <div>Hola <strong>Mundo</strong>!!</div>
    element.setData({name: 'Han'});
    // --> render: <div>Hola <strong>Han</strong>!!</div>
```

```javascript
    // Configuración del módulo
    kj.options.show_debug = true;
    // Carga de la plantilla
    await kj.templateLoader.loadBuffer('template_id');
    // Instancia un Element
    const element = new kj.Element(
        'div',                                          // Tipo del nodo principal
        kj.templateManager.getBuffer('template_id'),    // Buffer de plantilla
        {example: 'example'}                            // Datos
    );
    // Inserta element en la página
    document.body.appendChild(element.getNode());
```

### Consejos para mejorar el rendimiento.  

#### DomObserver.  

Habilitar la funcionalidad de DomObserver añade trigers que se disparan cada vez que el nodo se conecta o desconecta de la página, esto puede terner muchas apliacaciones prácticas, pero debe usarse únicamente si es necesario, ya que se incrementa mucho el tiempo de renderizado y la carga de trabajo del navegador, sobre todo si hay muchos elements funcionando al mismo tiempo.

#### Anidamiento de Elements.  

En una estructura de Elements en la que existe un control que a su vez tiene subcontroles, como por ejemplo, una lista en la que el UL es un Element y cada uno de los LI también, lo más óptimo es insertar el DOM de cada uno de los element hijos en el padre antes de insertar el padre en la página. 

#### Compilado de plantillas.  

Lo habitual será pasar al constructor de tu Element el buffer de la plantilla que va a usar, el se encargará de compilarla cuando lo necesite, pero en el caso de que tengas más de una instancia que use la misma plantilla, lo más adecuado, es que se compile primero y le pases al constructor de tus Elements la plantilla ya compilada, de esta manera no se compilará para cada una de las instancias que se genere.

#### Finalización de Elements.  

La clase Element tiene un procedimiento `terminate` que se ocupa de desbindear eventos y realizar alguna otra tarea de limpieza y eliminación de recursos, pero este procedimiento se tiene que ejecutar expresamente cuando llegue la hora de eliminar el Element de la página. Así mismo existe el procedimiento `onTerminate` y el callback `onTerminated` en los que se debería liberar todos aquellos recursos que se hayan empleado durante la vida útil del Element y que ya no serán necesarios.


#### Ejemplo
```javascript
class UL extends kj.Element {
    constructor() {
        super('div', kj.templateManager.getBuffer('ul'), null, {
            attr: {class: 'main', id: 'myElement', style: {color: 'red'}}
        });
    }

    /** @override */
    beforeInitialize() {
        // compila la plantilla una sola vez
        const lis_template = kj.templateManager.compile(kj.templateManager.getBuffer('inner_li'));
        this.lis = ['C3PO', 'R2D2', 'BB8'].map(name => new LI(lis_template, name));
    }

    /** @override */
    beforeBuild() {
        // Si queremos que se regeneren los LIs cuando lo haga el UL
        this.lis.forEach(li => li.build());
    }

    /** @override */
    afterBuild() {
        // Se apendizan los LIs al UL, en este punto el UL aún no está en la página
         kj(this.getNode().getElementsByTagName('ul')).item(0, ul => {
            this.lis.forEach(li => li.getNode() && ul.appendChild(li.getNode()));
        });
    }

    /** @override */
    onTerminate() {
        // Finaliza todos los elements hijos
        this.lis.forEach(li => li.terminate());
        this.lis.splice(0);
    }
}

class LI extends kj.Element {
    constructor(lis_template, name) {
        super('li', lis_template, { name: name });
    }
}

let lista = null;

document.addEventListener('DOMContentLoaded', async () => {
    // Carga de plantillas
    await kj.templateLoader.loadBuffer('ul', 'inner_li');
    // Creacion de Elements
    lista = new UL();
    // Inserción de lista
    kj('#resultHere', node => node.appendChild(lista.getNode()));
});
window.addEventListener('unload', () => {
    // Finalizando el Element al salir de la página
    lista.terminate();
});
```
Template: ul
```html
<h2>Ejemplo de Element con sub-Elements</h2>
<ul></ul>
```
Template: inner_li
```html
Hola soy <strong>{{name}}</strong>!!
```
Resultado
```html
<div class="main" id="myElement" style="color: red">
    <h2>Ejemplo de widget con sub-widgets</h2>
    <ul>
        <li>Hola soy <strong>C3PO</strong>!!</li>
        <li>Hola soy <strong>R2D2</strong>!!</li>
        <li>Hola soy <strong>BB8</strong>!!</li>
    </ul>
</div>
```

### Inline bindings.

@TODO

### El manipulador de dom integrado

@TODO

#### Plugins

@TODO

### Dependencias

- [handlebars](https://handlebarsjs.com/)

### Licencia

Copyright (c) 2018 Oscar Medina

Licensed under the [MIT License](https://gitlab.com/oscar-civic/kj/blob/master/LICENSE)
