# Changelog
**Relación de los cambios significativos realizados en la librería kj**

## [Unreleased]

- En la versión 2.0 se eliminará el soporte para `DomObserver`   
- EN la versión 2.0 se elimina soporte para subscripciones y todos los procedimientos relacionados  


## [1.1.0] - 2021-05-25
### Removed
- Se elimina soporte para subscripciones y todos los procedimientos relacionados  
    - `registerSubscription`  
    - `unregisterSubscription`  
    - `removeSubscriptions`  
    - `addSubscription`


### Added
- Mejora de rendimiento con `Initialize` implícito en el procedimiento `build`  
- Sistema de mesajería interna, se añaden los procedimientos
    - `postSystemMessage`  
    - `onSystemMessage`  
    - `getRunningElements`  
